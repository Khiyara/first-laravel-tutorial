</<!DOCTYPE html>
<html>
<head>
 
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}
 
input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}
 
input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}
 
input[type=submit]:hover {
  background-color: #45a049;
}
 
.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}
</style>
   
</head>
<body>
 
<h1>Create New Car</h1>
 
<div class="container">
 
    <form method="POST" action="/cars/create">
 
        @csrf
 
       <div>
          <label >Car Name</label>
          <input type="text" name="name" placeholder="Car Name" required>
 
      </div>
      <div>
            <label >Car Model</label>
            <textarea name="model" placeholder="Car Model" required></textarea>
 
      </div>
      <div>
            <label >Car Produced On</label>
            <input type="date" name="date" placeholder="Date Produce" required></input>
 
      </div>
      <div>
 
            <input type="submit" value="Make Car">
 
      </div>
 
    </form>  
 
 </div>   
 
</body>
</html>

